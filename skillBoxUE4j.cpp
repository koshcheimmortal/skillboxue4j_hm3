﻿// Практическая работа 19.5

#include <iostream>
#include <string>
#include "HeraldOfTheOldGods.h"

using namespace std;

class Animal {
protected:
	string Sound;
	Animal() : Sound("...") {
	}

public:

	virtual void Voice() {
		cout << "\nAnimal said: " << Sound;
	}
};

class Cat : public Animal {
public:
	Cat() {
		Sound = "Mrr Mya!";
	}
	void Voice() override {
		cout << "\nCat said: " << Sound;
	}
};

class Cow : public Animal {
public:
	Cow() {
		Sound = "Moooooooooooooo!";
	}
	void Voice() override {
		cout << "\nCow said " << Sound;
	}
};

class Human : public Animal {
public:
	Human() {
		Sound = "I am a crypto investor!";
	}
	void Voice() override {
		cout << "\nHuman said: " << Sound;
	}
};

class Abomination : public Animal {
public:
	Abomination() {
		Sound = "*too quiet mumbling*";
	}
	void Voice() override {
		cout << "\nIt said: " << Sound;
		_6E3H07NM();
	}

};

int main() {
	cout << "Best friends gathered in a circle and talk.\n";
	Animal* TheBestestFriends[] = {new Cat, new Cow, new Human, new Abomination};
	for (short i = 0; i < 4; i++) {
		TheBestestFriends[i]->Voice();
	}
}